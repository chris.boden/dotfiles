#!/usr/bin/env bash

GPCWD=$(pwd)

if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$DOTFILE_PATH
    mv $HOME/.gitconfig $HOME/.gitconfig.bak
fi

cd $DOTFILE_PATH/dotfiles
for FILE in *; do
  ln -s "$DOTFILE_PATH/dotfiles/$FILE" "$HOME/.$FILE"
done;

cd $DOTFILE_PATH/files
for FILE in *; do
  ln -s "$DOTFILE_PATH/files/$FILE" "$HOME/$FILE"
done;
cd $GPCWD


if [ -f ~/.tmux.conf ] && ! [ -x "$(command -v tmux)" ]; then
  if [ -x "$(command -v install-packages)" ]; then
    sudo install-packages tmux
  else
    echo "Unable to install tmux"
  fi
fi

if [[ "$SHELL" == *"bash"* ]]; then
  sudo sh -c "cat $DOTFILE_PATH/rc-fix >> $HOME/.bash_profile"
fi

curl https://raw.githubusercontent.com/fishtown-analytics/dbt-completion.bash/master/dbt-completion.bash > $HOME/.dbt-completion.bash
if [[ "$SHELL" == *"bash"* ]]; then
  echo "source $DOTFILE_PATH/files/db-analytics.env" >> $HOME/.bashrc
  echo "source $DOTFILE_PATH/files/datapipeline.env" >> $HOME/.bashrc

  echo "source $HOME/.dbt-completion.bash" >> $HOME/.bash_profile
fi
if [[ "$SHELL" == *"zsh"* ]]; then
  echo "source $DOTFILE_PATH/files/db-analytics.env" >> $HOME/.zshrc
  echo "source $DOTFILE_PATH/files/datapipeline.env" >> $HOME/.zshrc

  echo "autoload -U +X compinit && compinit" >> $HOME/.zshrc
  echo "autoload -U +X bashcompinit && bashcompinit" >> $HOME/.zshrc
  echo "source $HOME/.dbt-completion.bash" >> $HOME/.zshrc
fi

#sudo chsh -s $(which zsh) $(whoami)
